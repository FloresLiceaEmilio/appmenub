package app.emiliofl.appmenubutton92

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import app.emiliofl.appmenubutton92.R


class ListaFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>
    lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val View = inflater.inflate(R.layout.fragment_lista, container, false)
        listView = View.findViewById(R.id.lstAlumnos)
        searchView = View.findViewById(R.id.idSV)

        val items = resources.getStringArray(R.array.alumnos)

        arrayList = java.util.ArrayList()
        arrayList.addAll(items)

        adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, arrayList)

        listView.adapter = adapter

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                if (arrayList.contains(query)) {
                    adapter.filter.filter(query)
                } else {
                    Toast.makeText(requireContext(), "No alumno encontrado", Toast.LENGTH_LONG)
                        .show()
                }
                return false
            }


            override fun onQueryTextChange(newText: String?): Boolean {
                val trimmedText = newText?.trim()?.toLowerCase()
                adapter.filter.filter(trimmedText)
                return false
            }
        })

        listView.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
            var alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de alumnos")
            builder.setMessage(position.toString() + ": " + alumno)
            builder.setPositiveButton("ok") { dialog, which ->
            }
            builder.show()
        })


        return View
    }

}