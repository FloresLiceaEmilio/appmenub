package app.emiliofl.appmenubutton92

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import app.emiliofl.appmenubutton92.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.btnNavigator)
        bottomNavigationView.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.btnhome -> {
                    cambiarFrame(HomeFragment())
                    true
                }

                R.id.btnlista -> {
                    cambiarFrame(ListaFragment())
                    true
                }

                R.id.btndata -> {
                    cambiarFrame(DbFragment())
                    true
                }

                R.id.btnacerca -> {
                    cambiarFrame(AcercaFragment())
                    true
                }

                else -> false
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    fun cambiarFrame(fragment: Fragment) {
        Log.d("MainActivity", "Cambiando a fragmento: ${fragment.javaClass.simpleName}")
        supportFragmentManager.beginTransaction().replace(R.id.frmContenedor, fragment).commit()
    }
}
