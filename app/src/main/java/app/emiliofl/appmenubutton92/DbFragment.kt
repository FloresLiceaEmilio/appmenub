package app.emiliofl.appmenubutton92

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import app.emiliofl.appmenubutton92.DataBase.Alumno
import app.emiliofl.appmenubutton92.DataBase.dbAlumnos
import app.emiliofl.appmenubutton92.R

private const val ARG_ALUMNO = "alumno"
private const val PICK_IMAGE_REQUEST = 1
private const val REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 2

class DbFragment : Fragment() {

    private var alumno: Alumno? = null

    private lateinit var btnBorrar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnAgrega: Button
    private lateinit var btnSubirFoto: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var imgAlumno: ImageView
    private lateinit var db: dbAlumnos

    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            alumno = it.getParcelable(ARG_ALUMNO)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        btnAgrega = view.findViewById(R.id.btnAgrega)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnSubirFoto = view.findViewById(R.id.btnSubirFoto)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        imgAlumno = view.findViewById(R.id.imgAlumno)

        alumno?.let {
            txtMatricula.setText(it.matricula)
            txtNombre.setText(it.nombre)
            txtDomicilio.setText(it.domicilio)
            txtEspecialidad.setText(it.especialidad)
            if (!it.foto.isNullOrEmpty()) {
                Glide.with(this)
                    .load(it.foto)
                    .placeholder(R.mipmap.imagen)
                    .error(R.mipmap.imagen)
                    .into(imgAlumno)
            } else {
                imgAlumno.setImageResource(R.mipmap.imagen)
            }
            btnBorrar.isEnabled = true // Enable the Borrar button if an alumno is passed
        } ?: run {
            btnBorrar.isEnabled = false // Disable the Borrar button if no alumno is passed
        }


        btnSubirFoto.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION_READ_EXTERNAL_STORAGE
                )
            } else {
                openImageChooser()
            }
        }

        btnAgrega.setOnClickListener {
            if (txtNombre.text.toString().isEmpty() ||
                txtDomicilio.text.toString().isEmpty() ||
                txtMatricula.text.toString().isEmpty() ||
                txtEspecialidad.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    requireContext(),
                    "Faltó información por capturar",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                db = dbAlumnos(requireContext())
                db.openDatabase()

                val isRegistered = db.matriculaExiste(txtMatricula.text.toString())
                val currentAlumno =
                    if (isRegistered) db.getAlumno(txtMatricula.text.toString()) else null

                val newImageUrl = imageUri?.toString()
                val updatedImageUrl =
                    if (!newImageUrl.isNullOrEmpty()) newImageUrl else currentAlumno?.foto

                val alumno = Alumno().apply {
                    nombre = txtNombre.text.toString()
                    matricula = txtMatricula.text.toString()
                    domicilio = txtDomicilio.text.toString()
                    especialidad = txtEspecialidad.text.toString()
                    if (updatedImageUrl != null) {
                        foto = updatedImageUrl
                    }
                }

                val msg: String
                val id: Long
                if (isRegistered) {
                    msg = "Estudiante con matrícula ${alumno.matricula} se actualizó."
                    db.actualizarAlumno(alumno, alumno.matricula)
                    id = 1
                } else {
                    id = db.insertarAlumno(alumno)
                    msg = "Se agregó con éxito con ID $id"
                    // clean
                    txtMatricula.setText("")
                    txtNombre.setText("")
                    txtDomicilio.setText("")
                    txtEspecialidad.setText("")
                    imgAlumno.setImageResource(R.mipmap.imagen)
                    imageUri = null
                }
                Toast.makeText(
                    requireContext(),
                    msg,
                    Toast.LENGTH_SHORT
                ).show()
                db.close()
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Faltó capturar matrícula",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                db = dbAlumnos(requireContext())
                db.openDatabase()
                val alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.id != 0) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    if (!alumno.foto.isNullOrEmpty()) {
                        Glide.with(this)
                            .load(alumno.foto)
                            .placeholder(R.mipmap.imagen)
                            .error(R.mipmap.imagen)
                            .into(imgAlumno)
                    } else {
                        imgAlumno.setImageResource(R.mipmap.imagen)
                    }
                    btnBorrar.isEnabled = true
                } else {
                    Toast.makeText(
                        requireContext(),
                        "No se encontró la matrícula",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                db.close()
            }
        }

        btnBorrar.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Faltó capturar matrícula",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Confirmación")
                builder.setMessage("¿Está seguro de que desea borrar al alumno con matrícula ${txtMatricula.text}?")
                builder.setPositiveButton("Sí") { dialog, _ ->
                    db = dbAlumnos(requireContext())
                    db.openDatabase()
                    val matricula = txtMatricula.text.toString()
                    val status = db.borrarAlumno(matricula)
                    if (status != 0) {
                        txtMatricula.setText("")
                        txtNombre.setText("")
                        txtDomicilio.setText("")
                        txtEspecialidad.setText("")
                        imgAlumno.setImageResource(R.mipmap.imagen)
                        btnBorrar.isEnabled = false
                        Toast.makeText(
                            requireContext(),
                            "Se borró el usuario con la matrícula $matricula",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "No se encontró la matrícula",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dialog.dismiss()
                    db.close()
                }
                builder.setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }
                val alert = builder.create()
                alert.show()
            }
        }

        if (alumno != null) {
            // Pre-fill the form with the passed Alumno data
            txtMatricula.setText(alumno?.matricula)
            txtNombre.setText(alumno?.nombre)
            txtDomicilio.setText(alumno?.domicilio)
            txtEspecialidad.setText(alumno?.especialidad)
            if (!alumno?.foto.isNullOrEmpty()) {
                Glide.with(this)
                    .load(alumno?.foto)
                    .placeholder(R.mipmap.imagen)
                    .error(R.mipmap.imagen)
                    .into(imgAlumno)
            } else {
                imgAlumno.setImageResource(R.mipmap.imagen)
            }
        }

        return view
    }

    private fun openImageChooser() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            imageUri = data.data
            Glide.with(this)
                .load(imageUri)
                .placeholder(R.mipmap.imagen)
                .error(R.mipmap.imagen)
                .into(imgAlumno)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_READ_EXTERNAL_STORAGE) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                openImageChooser()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Permiso denegado para acceder a archivos.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(alumno: Alumno) =
            DbFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ALUMNO, alumno)
                }
            }
    }

}